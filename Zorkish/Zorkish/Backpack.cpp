#include "Backpack.h"

void Backpack::Print()
{
	_inventory->Print();
}

void Backpack::Add(Equipment* item)
{
	_inventory->Add(item);
}

Equipment* Backpack::UseItem(string itemName)
{
	return _inventory->UseItem(itemName);
}

void Backpack::RemoveItem(string itemName)
{
	_inventory->RemoveItem(itemName);
}

void Backpack::LookAt()
{
	cout << "It is a backpack" << endl;
}

void Backpack::LookIn()
{
	cout << "The bag is empty" << endl;
}

Backpack::Backpack()
{
	setName("Backpack");
	_inventory = new Inventory();
}