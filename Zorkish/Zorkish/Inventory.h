#pragma once

#include <string>
#include <list>
#include <iostream>
#include "Equipment.h"

using namespace std;

class Inventory
{
public:
	Inventory();
	~Inventory();
	void Print();
	void Add(Equipment* item);
	Equipment* UseItem(string itemName);
	void RemoveItem(string itemName);
};