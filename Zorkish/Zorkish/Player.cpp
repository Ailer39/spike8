#include "Player.h"

Player::Player(Location *startLocation)
{
	_currentLocation = startLocation;
}

Location* Player::GetCurrentPosition()
{
	return _currentLocation;
}

void Player::MovePlayer(Direction direction)
{
	if (_currentLocation != nullptr)
	{
		Location* targetLocation = _currentLocation->GetLocation(direction);

		if (targetLocation != nullptr)
		{
			_currentLocation = targetLocation;
		}

		cout << "Current location: " << _currentLocation->GetName() << endl;
	}
}

void Player::PutIn(string command)
{
	int putStartIndex = command.find("put");
	int inStartIndex = command.find("in");

	string object = command.substr(putStartIndex + 3, inStartIndex - 3);
}