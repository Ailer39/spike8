#pragma once
#include <string>
#include "MoveCommand.h"
#include "LookCommand.h"
#include "PutInCommand.h"
#include <regex>
#include <map>

using namespace std;
using namespace std::regex_constants;

class CommandManager
{
private:
	Player* _player;
	map<string, BaseCommand*> _commands;
public:
	void ExecuteCommand(string command);
	CommandManager(Player* player);
};