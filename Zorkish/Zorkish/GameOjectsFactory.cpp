#include "GameOjectsFactory.h"

GameObject* GameOjectsFactory::createGameObject(string type)
{
	if (type == "Backpack")
	{
		return new Backpack();
	}

	return new GameObject(type);
}