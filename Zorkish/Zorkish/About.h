#pragma once
#include "StateBase.h"

class StateManager;
class About :
	public StateBase
{
private:
		About() {};

public:
	static About* GetInstance();
	void Show(StateManager* context);
};