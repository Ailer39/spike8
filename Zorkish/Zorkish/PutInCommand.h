#pragma once
#include "BaseCommand.h"
#include "Player.h"

class PutInCommand :
	public BaseCommand
{
private:
	Player* _player;
public:
	PutInCommand(Player* player)
	{
		_player = player;
	}

	void execute(Location* currentLocation = nullptr, string command = "")
	{
		_player->PutIn(command);
	}
};