#pragma once
#include <string>
#include <iostream>

using namespace std;

class GameObject
{
private:
	string _name;
protected:
	GameObject() {};
	void setName(string name);
public:
	string GetName();
	GameObject(string name);
};