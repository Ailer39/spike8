#include "Inventory.h"

list<Equipment*>* inventory;

void Inventory::Print()
{
	cout << endl;
	for (list<Equipment*>::iterator i = inventory->begin(); i != inventory->end(); i++)
	{
		cout << "Item: " << (*i)->GetName();
	}
}

void Inventory::Add(Equipment* item)
{
	inventory->insert(inventory->begin(), item);
}

Equipment* Inventory::UseItem(string itemName)
{
	Equipment* tmp = nullptr;
	for (list<Equipment*>::iterator i = inventory->begin(); i != inventory->end(); i++)
	{
		if ((*i)->GetName() == itemName)
		{
			tmp = (*i);
			break;
		}
	}

	return tmp;
}

void Inventory::RemoveItem(string itemName)
{
	Equipment* tmp = nullptr;
	for (list<Equipment*>::iterator i = inventory->begin(); i != inventory->end(); i++)
	{
		if ((*i)->GetName() == itemName)
		{
			tmp = (*i);
			break;
		}
	}

	inventory->remove(tmp);
}

Inventory::Inventory()
{
	inventory = new list<Equipment*>();
}

Inventory::~Inventory()
{
	delete inventory;
}