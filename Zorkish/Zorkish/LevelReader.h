#pragma once
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include "Location.h"
#include "rapidjson/document.h"
#include "GameOjectsFactory.h"


using namespace rapidjson;
using namespace std;

class LevelReader
{
private:
	string _path;
	vector<Location*> _locations;
	Location* GetLevel();
	vector<int> GetConnections();
	Location* FindLocationById(int id);
	const int maxLevelSize = 512;
	string ReadFile();
public:
	LevelReader(string path);
	vector<Location*> GetLevelGraph();
};