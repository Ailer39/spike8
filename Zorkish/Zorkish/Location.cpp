#include "Location.h"

void Location::AddLocation(Location* newLocation, int direction)
{
	_connectedNotes[direction] = newLocation;
}

Location* Location::GetLocation(Direction direction)
{
	if (_connectedNotes[direction] != nullptr)
	{
		return _connectedNotes[direction];
	}

	return nullptr;
}

void Location::AddGameObject(GameObject* gameObj)
{
	_gameObjects.insert(_gameObjects.begin(), gameObj);
}

void Location::RemoveGameObject(string name)
{
	vector<GameObject*>::iterator foundElement;
	for (vector<GameObject*>::iterator i = _gameObjects.begin(); i != _gameObjects.end(); i++)
	{
		if (name != (*i)->GetName())
		{
			foundElement = i;
			break;
		}
	}

	_gameObjects.erase(foundElement);
}

GameObject* Location::GetGameObject(string name)
{
	vector<GameObject*>::iterator foundElement;
	for (vector<GameObject*>::iterator i = _gameObjects.begin(); i != _gameObjects.end(); i++)
	{
		if (name != (*i)->GetName())
		{
			return (*i);
		}
	}

	return nullptr;
}

string Location::GetName()
{
	return _name;
}

int Location::GetId()
{
	return _id;
}

string Location::GetDescription()
{
	return _description;
}

void Location::PrintConnections()
{
	for (unsigned int i = 0; i < 4; i++)
	{
		if (_connectedNotes[i] != nullptr)
		{
			cout << "Door at ";

			switch (i)
			{
			case 0: cout << "North ";
				break;
			case 1: cout << "South ";
				break;
			case 2: cout << "East";
				break;
			case 3: cout << "West";
				break;
			default:
				break;
			}

			cout << endl;
		}
	}
}

Location::Location(int id, string name, string description)
{
	_name = name;
	_id = id;
	_description = description;
}

void Location::Look()
{
	cout << GetDescription() << endl;
	PrintConnections();

	for (vector<GameObject*>::iterator i = _gameObjects.begin(); i != _gameObjects.end(); i++)
	{
		cout << "You see a " << (*i)->GetName() << endl;
	}
}