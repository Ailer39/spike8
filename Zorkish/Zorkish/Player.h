#pragma once
#include "Location.h"
#include "Direction.h"
#include "Inventory.h"
#include <regex>

class Player
{
private:
	Location* _currentLocation;

public:
	void MovePlayer(Direction direction);
	Location* GetCurrentPosition();
	Player(Location* startLocation);
	void PutIn(string command);
};

