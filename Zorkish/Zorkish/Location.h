#pragma once

#include <string>
#include <vector>
#include <iostream>
#include "GameObject.h"
#include "Direction.h"
#include <regex>

using namespace std;

class Location
{
private:
	Location* _connectedNotes[4];
	vector<GameObject*> _gameObjects;
	string _name;
	string _description;
	int _id;
public:
	Location(int id, string name, string description);
	void AddLocation(Location* newLocation, int direction);
	void AddGameObject(GameObject* gameObj);
	void RemoveGameObject(string name);
	GameObject* GetGameObject(string name);
	Location* GetLocation(Direction direction);
	string GetName();
	string GetDescription();
	int GetId();
	void PrintConnections();
	void Look();
};